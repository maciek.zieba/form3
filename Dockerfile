FROM golang:1.11.1 AS builder

WORKDIR $GOPATH/src/gitlab.com/form3
COPY go.mod .
COPY go.sum .

ENV GO111MODULE "on"

RUN go mod download
COPY . ./

RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix nocgo -o /app .

FROM scratch
USER 1000
ENV FORM3_ENVIRONMENT "" # please set it up
COPY --from=builder /app ./
COPY config/ /config/
ENTRYPOINT ["./app"]
