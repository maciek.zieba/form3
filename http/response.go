package http

import "gitlab.com/maciek.zieba/form3/payment"

type (
	PaymentResponse struct {
		Uid    string `json:"uid"`
		Amount struct {
			Value        int64  `json:"value"`
			CurrencyCode string `json:"currencyCode"`
		} `json:"amount"`
		Method      string `json:"method"`
		Description string `json:"description"`
		OrderId     string `json:"orderId"`
		Status      string `json:"status"`
	}

	PaymentCollectionResponse struct {
		Items []PaymentResponse `json:"items"`
	}
)

func NewPaymentResponseFromPayment(p *payment.Payment) PaymentResponse {
	response := PaymentResponse{
		Uid:         p.Uid.String(),
		Method:      p.Method,
		Description: p.Description,
		OrderId:     p.OrderId,
		Status:      p.Status,
	}
	response.Amount.Value = p.Amount.Amount()
	response.Amount.CurrencyCode = p.Amount.Currency().Code
	return response
}

func NewPaymentCollectionResponse(collection []*payment.Payment) PaymentCollectionResponse {
	response := PaymentCollectionResponse{
		Items: make([]PaymentResponse, 0),
	}
	for _, p := range collection {
		response.Items = append(response.Items, NewPaymentResponseFromPayment(p))
	}
	return response
}
