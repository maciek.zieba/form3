package http

import (
	"encoding/json"
	"fmt"
	"github.com/labstack/echo"
	log "github.com/sirupsen/logrus"
	"gitlab.com/maciek.zieba/form3/payment"
	"net/http"
)

func HandleGetPayments(service *payment.Service) func(c echo.Context) error {
	return func(c echo.Context) error {
		collection, err := service.GetPayments(c.Request().Context())
		if err != nil {
			switch err.(type) {
			case payment.InfrastructureError:
				log.Errorf(err.Error())
				return c.JSON(http.StatusInternalServerError, fmt.Sprintf("unable to fetch payments collection"))
			case payment.DomainError:
				return c.JSON(http.StatusBadRequest, err.Error())
			default:
				return c.JSON(http.StatusInternalServerError, nil)
			}
		}
		return c.JSON(http.StatusOK, NewPaymentCollectionResponse(collection))
	}
}

func HandleGetSinglePayment(service *payment.Service) func(c echo.Context) error {
	return func(c echo.Context) error {
		uid := c.Param("uid")
		paymentEntity, err := service.GetPaymentByUid(c.Request().Context(), uid)
		if err != nil {
			switch err.(type) {
			case payment.InfrastructureError:
				log.Errorf(err.Error())
				return c.JSON(http.StatusInternalServerError, fmt.Sprintf("unable to fetch payment %s", uid))
			case payment.DomainError:
				return c.JSON(http.StatusBadRequest, err.Error())
			default:
				return c.JSON(http.StatusInternalServerError, nil)
			}
		}
		if paymentEntity == nil {
			return c.JSON(http.StatusNotFound, fmt.Sprintf("Payment %s not found", uid))
		}
		return c.JSON(http.StatusOK, NewPaymentResponseFromPayment(paymentEntity))
	}
}

func HandleCreatePayment(service *payment.Service) func(c echo.Context) error {
	return func(c echo.Context) error {
		newPaymentDTO, err := createNewPaymentDTO(c.Request())
		if err != nil {
			return c.JSON(http.StatusBadRequest, err.Error())
		}
		newPayment, err := service.CreateNewPayment(c.Request().Context(), newPaymentDTO)
		if err != nil {
			switch err.(type) {
			case payment.InfrastructureError:
				log.Errorf(err.Error())
				return c.JSON(http.StatusInternalServerError, "unable to persist payment")
			case payment.DomainError:
				return c.JSON(http.StatusBadRequest, err.Error())
			default:
				return c.JSON(http.StatusInternalServerError, nil)
			}
		}
		c.Response().Header().Set(echo.HeaderLocation, fmt.Sprintf("/v1/payment/%s", newPayment.Uid))
		return c.NoContent(http.StatusCreated)
	}
}

func HandleDeletePayment(service *payment.Service) func(c echo.Context) error {
	return func(c echo.Context) error {
		uid := c.Param("uid")
		err := service.DeletePaymentByUid(c.Request().Context(), uid)
		if err != nil {
			switch err.(type) {
			case payment.InfrastructureError:
				log.Errorf(err.Error())
				return c.JSON(http.StatusInternalServerError, fmt.Sprintf("unable to delete payment %s", uid))
			case payment.DomainError:
				return c.JSON(http.StatusBadRequest, err.Error())
			default:
				return c.JSON(http.StatusInternalServerError, nil)
			}
		}
		return c.NoContent(http.StatusNoContent)
	}
}

func createNewPaymentDTO(req *http.Request) (payment.NewPaymentDTO, error) {
	decoder := json.NewDecoder(req.Body)
	var dto payment.NewPaymentDTO
	err := decoder.Decode(&dto)
	if err != nil {
		return payment.NewPaymentDTO{}, fmt.Errorf("unable to parse request json")
	}
	err = dto.Validate()
	if err != nil {
		return payment.NewPaymentDTO{}, err
	}
	return dto, nil
}
