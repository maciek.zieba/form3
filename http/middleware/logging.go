package middleware

import (
	"fmt"
	"github.com/labstack/echo"
	log "github.com/sirupsen/logrus"
	"time"
)

func LoggerMiddleware() echo.MiddlewareFunc {
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			req := c.Request()
			res := c.Response()
			start := time.Now()

			var err error
			if err = next(c); err != nil {
				c.Error(err)
			}
			stop := time.Now()
			logStr := fmt.Sprintf("%s %d [%s] %s%s %13v %s %s",
				c.RealIP(),
				res.Status,
				req.Method,
				req.Host,
				req.RequestURI,
				stop.Sub(start).String(),
				req.Referer(),
				req.UserAgent(),
			)
			if res.Status < 300 {
				log.Infof(logStr)
			} else if res.Status >= 400 && res.Status < 500 {
				log.Warnf(logStr)
			} else {
				log.Errorf(logStr)
			}

			return err
		}
	}
}
