package http

import (
	"context"
	"github.com/labstack/echo"
	"github.com/stretchr/testify/assert"
	"gitlab.com/maciek.zieba/form3/payment"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
)

func TestHandleGetPayments(t *testing.T) {
	// Setup
	e := echo.New()
	req := httptest.NewRequest(http.MethodGet, "/v1/payments", nil)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)

	repo := payment.NewInMemoryRepository()
	repo.LoadFakeData()

	service := payment.NewService(repo)
	handler := HandleGetPayments(service)
	// Assertions
	if assert.NoError(t, handler(c)) {
		assert.Equal(t, http.StatusOK, rec.Code)
	}
}

func TestHandleGetSinglePaymentWrongUuidFormat(t *testing.T) {
	// Setup
	e := echo.New()
	req := httptest.NewRequest(http.MethodGet, "/v1/payments/not_valid_uuid", nil)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)
	c.SetPath("/v1/payments/not_valid_uuid")
	c.SetParamNames("uid")
	c.SetParamValues("not_valid_uuid")

	repo := payment.NewInMemoryRepository()
	service := payment.NewService(repo)
	handler := HandleGetSinglePayment(service)
	// Assertions
	if assert.NoError(t, handler(c)) {
		assert.Equal(t, http.StatusBadRequest, rec.Code)
	}
}

func TestHandleGetSinglePaymentNotExistingPayment(t *testing.T) {
	// Setup
	e := echo.New()
	req := httptest.NewRequest(http.MethodGet, "/v1/payments", nil)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)

	c.SetPath("/v1/payments/9bc354d2-5942-48a1-a5f6-d7541d2f524e")
	c.SetParamNames("uid")
	c.SetParamValues("9bc354d2-5942-48a1-a5f6-d7541d2f524e")

	repo := payment.NewInMemoryRepository()
	service := payment.NewService(repo)
	handler := HandleGetSinglePayment(service)
	// Assertions
	if assert.NoError(t, handler(c)) {
		assert.Equal(t, http.StatusNotFound, rec.Code)
	}
}

func TestHandleGetSinglePayment(t *testing.T) {
	e := echo.New()
	req := httptest.NewRequest(http.MethodGet, "/v1/payments", nil)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)

	c.SetPath("/v1/payments/9bc359744ef95-4890-4a45-b860-5e82dd451ec8")
	c.SetParamNames("uid")
	c.SetParamValues("9744ef95-4890-4a45-b860-5e82dd451ec8")

	repo := payment.NewInMemoryRepository()
	repo.LoadFakeData()
	service := payment.NewService(repo)
	handler := HandleGetSinglePayment(service)
	// Assertions
	if assert.NoError(t, handler(c)) {
		assert.Equal(t, http.StatusOK, rec.Code)
	}
}

func TestHandleCreatePayment(t *testing.T) {
	e := echo.New()

	body := `{
  "amount": {
    "value": 2989,
    "currencyCode": "EUR"
  },
  "method": "CreditCard",
  "description": "string",
  "orderId": "4910239571"
}`
	bodyReader := strings.NewReader(body)

	req := httptest.NewRequest(http.MethodPost, "/v1/payments", bodyReader)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)

	c.SetPath("/v1/payments")

	repo := payment.NewInMemoryRepository()
	service := payment.NewService(repo)
	handler := HandleCreatePayment(service)
	// Assertions
	if assert.NoError(t, handler(c)) {
		assert.Equal(t, http.StatusCreated, rec.Code)
		assert.NotEmpty(t, rec.Header().Get("Location"))
		list, _ := repo.FetchAllPayments(context.TODO())
		assert.Equal(t, 1, len(list))
	}
}

func TestHandleCreatePaymentBadRequest(t *testing.T) {
	e := echo.New()

	body := `{
  "amount": {
  "method": "CreditCard",
  "description": "string",
  "orderId": "4910239571"
}`
	bodyReader := strings.NewReader(body)

	req := httptest.NewRequest(http.MethodPost, "/v1/payments", bodyReader)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)

	c.SetPath("/v1/payments")

	repo := payment.NewInMemoryRepository()
	service := payment.NewService(repo)
	handler := HandleCreatePayment(service)
	// Assertions
	if assert.NoError(t, handler(c)) {
		assert.Equal(t, http.StatusBadRequest, rec.Code)
	}
}

func TestHandleDeletePaymentBadRequest(t *testing.T) {
	e := echo.New()
	req := httptest.NewRequest(http.MethodDelete, "/v1/payments", nil)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)

	c.SetPath("/v1/payments/9bc354d2-5942-48a1-a5f6-d7541d2f524e")
	c.SetParamNames("uid")
	c.SetParamValues("9bc354d2-5942-48a1-a5f6-d7541d2f524e")

	repo := payment.NewInMemoryRepository()
	service := payment.NewService(repo)
	handler := HandleDeletePayment(service)
	// Assertions
	if assert.NoError(t, handler(c)) {
		assert.Equal(t, http.StatusBadRequest, rec.Code)
	}
}

func TestHandleDeletePayment(t *testing.T) {
	e := echo.New()
	req := httptest.NewRequest(http.MethodDelete, "/v1/payments", nil)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)

	c.SetPath("/v1/payments/9744ef95-4890-4a45-b860-5e82dd451ec8")
	c.SetParamNames("uid")
	c.SetParamValues("9744ef95-4890-4a45-b860-5e82dd451ec8")

	repo := payment.NewInMemoryRepository()
	repo.LoadFakeData()
	service := payment.NewService(repo)
	handler := HandleDeletePayment(service)
	// Assertions
	if assert.NoError(t, handler(c)) {
		assert.Equal(t, http.StatusNoContent, rec.Code)
		list, _ := repo.FetchAllPayments(context.TODO())
		assert.Equal(t, 1, len(list))
	}
}

func TestCreateNewPaymentDTO(t *testing.T) {
	body := `{
  "amount": {
    "value": 2989,
    "currencyCode": "EUR"
  },
  "method": "CreditCard",
  "description": "string",
  "orderId": "4910239571"
}`
	bodyReader := strings.NewReader(body)
	req, _ := http.NewRequest("POST", "/", bodyReader)
	dto, err := createNewPaymentDTO(req)
	assert.Nil(t, err)
	assert.Equal(t, int64(2989), dto.Amount.Value)
	assert.Equal(t, "EUR", dto.Amount.CurrencyCode)
	assert.Equal(t, "CreditCard", dto.Method)
	assert.Equal(t, "4910239571", dto.OrderId)
	assert.Equal(t, "string", dto.Description)
}

func TestCreateNewPaymentDTONotValid(t *testing.T) {
	body := `{
  "amount": {
    "value": -2989,
    "currencyCode": "EUR"
  },
  "method": "CreditCard",
  "description": "string",
  "orderId": "4910239571"
}`
	bodyReader := strings.NewReader(body)
	req, _ := http.NewRequest("POST", "/", bodyReader)
	_, err := createNewPaymentDTO(req)
	assert.NotNil(t, err)
}

func TestCreateNewPaymentDTOBadJson(t *testing.T) {
	body := `{
  "amount": {{
    value: 2989,
    "currencyCode": "EUR"
  },
  "method": "CreditCard",
  "description": "string",
  "orderId": "4910239571"
}`
	bodyReader := strings.NewReader(body)
	req, _ := http.NewRequest("POST", "/", bodyReader)
	_, err := createNewPaymentDTO(req)
	assert.NotNil(t, err)
}
