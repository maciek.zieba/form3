package http

import (
	"github.com/Rhymond/go-money"
	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"gitlab.com/maciek.zieba/form3/payment"
	"testing"
)

func TestNewPaymentResponseFromPayment(t *testing.T) {
	uid, _ := uuid.Parse("e7f789b5-8616-4641-823d-842a09cdfc85")
	p := &payment.Payment{
		Uid:         uid,
		Method:      "PayPal",
		Status:      "Complete",
		OrderId:     "123",
		Description: "test_description",
		Amount:      money.New(1399, "EUR"),
	}
	response := NewPaymentResponseFromPayment(p)
	assert.Equal(t, "e7f789b5-8616-4641-823d-842a09cdfc85", response.Uid)
	assert.Equal(t, "PayPal", response.Method)
	assert.Equal(t, "Complete", response.Status)
	assert.Equal(t, "123", response.OrderId)
	assert.Equal(t, "test_description", response.Description)
	assert.Equal(t, int64(1399), response.Amount.Value)
	assert.Equal(t, "EUR", response.Amount.CurrencyCode)
}

func TestNewPaymentCollectionResponse(t *testing.T) {
	p1 := &payment.Payment{
		Uid:         uuid.UUID{},
		Method:      "PayPal",
		Status:      "Complete",
		OrderId:     "123",
		Description: "test_description_p1",
		Amount:      money.New(999, "USD"),
	}
	p2 := &payment.Payment{
		Uid:         uuid.UUID{},
		Method:      "PayPal",
		Status:      "Complete",
		OrderId:     "321",
		Description: "test_description_p2",
		Amount:      money.New(1399, "EUR"),
	}

	collection := []*payment.Payment{p1, p2}
	response := NewPaymentCollectionResponse(collection)
	assert.Equal(t, 2, len(response.Items))
}
