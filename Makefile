build:
	GO111MODULE=on go mod download
	go build -o bin/products-api

run: build
	FORM3_ENVIRONMENT=dev ./bin/products-api

test:
	GO111MODULE=on go mod download && go test ./... -v

run-in-docker:
	docker-compose build
	docker-compose up