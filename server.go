package main

import (
	"database/sql"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"gitlab.com/maciek.zieba/form3/http"
	customMiddleware "gitlab.com/maciek.zieba/form3/http/middleware"
	"gitlab.com/maciek.zieba/form3/payment"
	"os"
	"time"
)

type Server struct {
	config *viper.Viper
	port   int
	echo   *echo.Echo
	db     *sql.DB
}

func NewServer(env string) *Server {
	log.SetFormatter(&log.TextFormatter{
		FullTimestamp: true,
	})
	log.SetOutput(os.Stdout)
	log.SetLevel(log.InfoLevel)
	if env == "dev" {
		log.SetLevel(log.DebugLevel)
	}

	// Conf init
	config, err := createConfig(env)
	if err != nil {
		log.Fatalf("unable to create config: %s", err)
	}

	db, err := sql.Open("mysql", config.GetString("sql_dsn"))
	if err != nil {
		log.Fatalf("unable to connect to db: %s", err)
	}
	db.SetMaxOpenConns(10)
	db.SetMaxIdleConns(10)
	db.SetConnMaxLifetime(5 * time.Second)
	err = db.Ping()
	if err != nil {
		log.Fatalf("unable to ping db: %s", err)
	}

	server := &Server{}
	server.config = config
	server.port = 3001
	server.db = db

	return server
}

func (server *Server) Run() {
	server.echo = echo.New()

	server.echo.Use(
		customMiddleware.LoggerMiddleware(),
		middleware.CORS(),
	)
	err := server.setupRouting()
	if err != nil {
		log.Fatalf("Unable to setup handlers: %s", err)
	}
	err = server.echo.Start(fmt.Sprintf(":%d", server.port))
	if err != nil {
		log.Fatalf("Server error: %s", err)
	}
}

func (server *Server) setupRouting() error {
	service := payment.NewService(payment.NewMysqlRepository(server.db))
	server.echo.POST("/v1/payments", http.HandleCreatePayment(service))
	server.echo.GET("/v1/payments/:uid", http.HandleGetSinglePayment(service))
	server.echo.GET("/v1/payments", http.HandleGetPayments(service))
	server.echo.DELETE("/v1/payments/:uid", http.HandleDeletePayment(service))

	return nil
}

func createConfig(env string) (*viper.Viper, error) {
	c := viper.New()

	c.SetConfigName("config")
	c.AddConfigPath(fmt.Sprintf("./config/%s", env))
	c.SetConfigType("yaml")
	c.AutomaticEnv()

	err := c.ReadInConfig()
	if err != nil {
		return nil, fmt.Errorf("fatal error reading config file: %s \n", err)
	}

	return c, nil
}
