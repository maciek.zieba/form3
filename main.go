package main

import (
	"os"
)

func main() {
	env := os.Getenv("FORM3_ENVIRONMENT")
	if env == "" {
		panic("FORM3_ENVIRONMENT is empty")
	}
	server := NewServer(env)
	server.Run()
}
