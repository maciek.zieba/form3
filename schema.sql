CREATE DATABASE IF NOT EXISTS form3;
USE form3;
CREATE TABLE IF NOT EXISTS payment (
   uid  VARCHAR(255) NOT NULL,
   amount INT NOT NULL,
   currency  VARCHAR(16) NOT NULL,
   description  TEXT NOT NULL ,
   order_id   VARCHAR(128) NOT NULL,
   method VARCHAR(32) NOT NULL,
   status VARCHAR(32) NOT NULL,
   PRIMARY KEY (uid)
);