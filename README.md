# Documentation
Api is designed in OpenApi v3, you can find it here: 
https://gitlab.com/maciek.zieba/form3/blob/master/openapi/payments.yaml

export to PDF you can find here:
https://gitlab.com/maciek.zieba/form3/blob/master/openapi/apidoc.pdf

export to HTML yuu can find here:
https://gitlab.com/maciek.zieba/form3/raw/master/openapi/apidoc.html

# How to run
Configuration is in config/$FORM3_ENVIRONMENT/config.yaml file, you need to just prepare sql_dsn.

For example:
```
sql_dsn: 'root:root@tcp(mysql:3306)/form3?parseTime=true'
```
To run app locally please set
```
FORM3_ENVIRONMENT=dev
``` 
and prepare config/dev/config.yaml, then just run:
```
make run
```
App exposes port 3001

To run unit tests
```
make test
```

If you have docker and docker-compose you can run just (without previous steps):
```
make run-in-docker
```
It will prepare mysql and app. 

Endpoints will be available under:

http://localhost:3001/v1/payments (on linux)
or 
http://your_docker_host:3001/v1/payments (on VM, dinghy or other)
