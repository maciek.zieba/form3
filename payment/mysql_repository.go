package payment

import (
	"context"
	"database/sql"
	"fmt"
	"github.com/Rhymond/go-money"
	"github.com/google/uuid"
)

type MysqlRepository struct {
	db *sql.DB
}

func NewMysqlRepository(db *sql.DB) *MysqlRepository {
	return &MysqlRepository{
		db: db,
	}
}

func (r *MysqlRepository) PersistPayment(ctx context.Context, p *Payment) error {
	if p.Amount == nil {
		return fmt.Errorf("unable to store payment without amount")
	}
	query := `INSERT payment SET uid=? , amount=? , currency=?, status=? , order_id=?, method=?, description=?`
	stmt, err := r.db.PrepareContext(ctx, query)
	if err != nil {
		return err
	}
	_, err = stmt.ExecContext(ctx, p.Uid, p.Amount.Amount(), p.Amount.Currency().Code,
		p.Status, p.OrderId, p.Method, p.Description)
	if err != nil {
		return err
	}
	return nil
}

func (r *MysqlRepository) FetchPaymentByUid(ctx context.Context, uid uuid.UUID) (*Payment, error) {

	query := `SELECT uid, amount, currency, status, order_id, description, method
  						FROM payment WHERE uid = ?`
	rows, err := r.db.QueryContext(ctx, query, uid.String())
	if err != nil {
		return nil, err
	}
	result, err := r.fetch(rows)
	if err != nil {
		return nil, err
	}

	if len(result) > 0 {
		return result[0], nil
	}
	return nil, nil
}

func (r *MysqlRepository) FetchAllPayments(ctx context.Context) ([]*Payment, error) {
	query := `SELECT uid, amount, currency, status, order_id, description, method
  						FROM payment`
	rows, err := r.db.QueryContext(ctx, query)
	if err != nil {
		return nil, err
	}
	result, err := r.fetch(rows)
	if err != nil {
		return nil, err
	}

	return result, nil
}

func (r *MysqlRepository) DeletePaymentByUid(ctx context.Context, uid uuid.UUID) error {
	query := `DELETE FROM payment WHERE uid = ?`
	stmt, err := r.db.PrepareContext(ctx, query)
	if err != nil {
		return err
	}
	_, err = stmt.ExecContext(ctx, uid.String())
	if err != nil {
		return err
	}
	return nil
}

func (r *MysqlRepository) fetch(rows *sql.Rows) ([]*Payment, error) {
	result := make([]*Payment, 0)
	var amountValue int64
	var amountCurrency string
	var uidAsString string
	for rows.Next() {
		p := &Payment{}
		err := rows.Scan(
			&uidAsString,
			&amountValue,
			&amountCurrency,
			&p.Status,
			&p.OrderId,
			&p.Description,
			&p.Method,
		)
		if err != nil {
			return result, err
		}
		uid, err := uuid.Parse(uidAsString)
		if err != nil {
			return result, err
		}
		p.Uid = uid
		p.Amount = money.New(amountValue, amountCurrency)
		result = append(result, p)
	}
	err := rows.Close()
	if err != nil {
		return result, err
	}
	return result, nil
}
