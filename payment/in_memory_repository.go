package payment

import (
	"context"
	"fmt"
	"github.com/Rhymond/go-money"
	"github.com/google/uuid"
	"sync"
)

type InMemoryRepository struct {
	data map[string]*Payment
	mux  sync.Mutex
}

func NewInMemoryRepository() *InMemoryRepository {
	return &InMemoryRepository{
		data: make(map[string]*Payment),
	}
}

func (r *InMemoryRepository) PersistPayment(ctx context.Context, p *Payment) error {
	r.mux.Lock()
	r.data[p.Uid.String()] = p
	r.mux.Unlock()
	return nil
}

func (r *InMemoryRepository) FetchPaymentByUid(ctx context.Context, uid uuid.UUID) (*Payment, error) {
	r.mux.Lock()
	defer r.mux.Unlock()

	if p, ok := r.data[uid.String()]; ok {
		return p, nil
	}
	return nil, nil
}

func (r *InMemoryRepository) FetchAllPayments(ctx context.Context) ([]*Payment, error) {
	r.mux.Lock()
	defer r.mux.Unlock()

	collection := make([]*Payment, 0)
	for _, p := range r.data {
		collection = append(collection, p)
	}
	return collection, nil
}

func (r *InMemoryRepository) DeletePaymentByUid(ctx context.Context, uid uuid.UUID) error {
	r.mux.Lock()
	defer r.mux.Unlock()

	if _, ok := r.data[uid.String()]; ok {
		delete(r.data, uid.String())
		return nil
	}
	return fmt.Errorf("payment %s not present in repository", uid.String())
}

func (r *InMemoryRepository) LoadFakeData() {
	p1Uid, _ := uuid.Parse("e7f789b5-8616-4641-823d-842a09cdfc85")
	p2Uid, _ := uuid.Parse("9744ef95-4890-4a45-b860-5e82dd451ec8")

	p1 := &Payment{
		Uid:         p1Uid,
		Description: "test_description1",
		OrderId:     "p2Uid",
		Status:      "Pending",
		Method:      "CreditCard",
		Amount:      money.New(1199, "EUR"),
	}
	p2 := &Payment{
		Uid:         p2Uid,
		Description: "test_description2",
		OrderId:     "5132123",
		Status:      "Complete",
		Method:      "PayPal",
		Amount:      money.New(64887, "USD"),
	}

	r.PersistPayment(context.TODO(), p1)
	r.PersistPayment(context.TODO(), p2)
}
