package payment

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestNewPaymentDTO_Validate(t *testing.T) {
	dto := NewPaymentDTO{
		Method:  "PayPal",
		OrderId: "123",
	}
	dto.Amount.Value = -20
	dto.Amount.CurrencyCode = "EUR"
	err := dto.Validate()
	assert.NotNil(t, err)

	dto = NewPaymentDTO{
		Method:  "PayPal",
		OrderId: "123",
	}
	dto.Amount.Value = 20
	dto.Amount.CurrencyCode = "E"
	err = dto.Validate()
	assert.NotNil(t, err)

	dto = NewPaymentDTO{
		Method:  "",
		OrderId: "123",
	}
	dto.Amount.Value = 20
	dto.Amount.CurrencyCode = "EUR"
	err = dto.Validate()
	assert.NotNil(t, err)

	dto = NewPaymentDTO{
		Method:  "PayPal",
		OrderId: "",
	}
	dto.Amount.Value = 20
	dto.Amount.CurrencyCode = "EUR"
	err = dto.Validate()
	assert.NotNil(t, err)

	dto = NewPaymentDTO{
		Method:  "PayPal",
		OrderId: "123",
	}
	dto.Amount.Value = 20
	dto.Amount.CurrencyCode = "EUR"
	err = dto.Validate()
	assert.Nil(t, err)
}
