package payment

import (
	"context"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestService_IsPaymentMethodAllowed(t *testing.T) {
	assert.True(t, IsPaymentMethodAllowed("CreditCard"))
	assert.True(t, IsPaymentMethodAllowed("PayPal"))
	assert.False(t, IsPaymentMethodAllowed("SomeOther"))
}

func TestService_GetPaymentByUid(t *testing.T) {
	repo := NewInMemoryRepository()
	repo.LoadFakeData()
	service := NewService(repo)

	p, err := service.GetPaymentByUid(context.TODO(), "not_valid_uuid")
	assert.Nil(t, p)
	assert.NotNil(t, err)
	assert.IsType(t, DomainError{}, err)

	p, err = service.GetPaymentByUid(context.TODO(), "e7f789b5-8616-4641-823d-842a09cdfc85")
	assert.NotNil(t, p)
	assert.Nil(t, err)
	assert.Equal(t, "e7f789b5-8616-4641-823d-842a09cdfc85", p.Uid.String())
}

func TestService_GetPayments(t *testing.T) {
	repo := NewInMemoryRepository()
	repo.LoadFakeData()
	service := NewService(repo)
	collection, err := service.GetPayments(context.TODO())
	assert.Nil(t, err)
	assert.Equal(t, 2, len(collection))
}

func TestService_CreateNewPayment(t *testing.T) {
	repo := NewInMemoryRepository()
	service := NewService(repo)

	// Lets check not allowed method domain logic
	dto := NewPaymentDTO{
		Method:      "SomeNotAllowedMethod",
		OrderId:     "123",
		Description: "test_description",
	}
	dto.Amount.Value = 999
	dto.Amount.CurrencyCode = "EUR"
	p, err := service.CreateNewPayment(context.TODO(), dto)
	assert.Nil(t, p)
	assert.NotNil(t, err)
	assert.IsType(t, DomainError{}, err)

	dto = NewPaymentDTO{
		Method:      "PayPal",
		OrderId:     "123",
		Description: "test_description",
	}
	dto.Amount.Value = 999
	dto.Amount.CurrencyCode = "EUR"
	p, err = service.CreateNewPayment(context.TODO(), dto)
	assert.Nil(t, err)
	assert.NotNil(t, p)
	assert.Equal(t, "PayPal", p.Method)
	assert.Equal(t, "test_description", p.Description)
	assert.Equal(t, "123", p.OrderId)
	assert.Equal(t, int64(999), p.Amount.Amount())
	assert.Equal(t, "EUR", p.Amount.Currency().Code)

	// and it should appear in repository
	p, err = repo.FetchPaymentByUid(context.TODO(), p.Uid)
	assert.Nil(t, err)
	assert.NotNil(t, p)
}

func TestService_DeletePaymentByUid(t *testing.T) {
	repo := NewInMemoryRepository()
	repo.LoadFakeData()
	service := NewService(repo)

	err := service.DeletePaymentByUid(context.TODO(), "e7f789b5-8616-4641-823d-842a09cdfc85")
	assert.Nil(t, err)
	collection, err := repo.FetchAllPayments(context.TODO())
	assert.Equal(t, 1, len(collection))

	err = service.DeletePaymentByUid(context.TODO(), "61d03205-9bab-434f-9069-c10ed85c5cb3")
	assert.NotNil(t, err)

}
