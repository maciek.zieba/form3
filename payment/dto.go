package payment

import (
	"fmt"
)

type NewPaymentDTO struct {
	Amount struct {
		Value        int64  `json:"value"`
		CurrencyCode string `json:"currencyCode"`
	} `json:"amount"`
	Method      string `json:"method"`
	Description string `json:"description"`
	OrderId     string `json:"orderId"`
}

func (p NewPaymentDTO) Validate() error {
	if p.Amount.Value <= 0 {
		return fmt.Errorf("amount value should be > 0")
	}
	if len(p.Amount.CurrencyCode) < 2 {
		return fmt.Errorf("currencyCode should have at least 2 characters")
	}
	if len(p.Method) == 0 {
		return fmt.Errorf("payment method is requeried")
	}
	if len(p.OrderId) == 0 {
		return fmt.Errorf("orderId is requeried")
	}
	return nil
}
