package payment

import (
	"context"
	"github.com/google/uuid"
)

type Repository interface {
	PersistPayment(ctx context.Context, payment *Payment) error
	FetchPaymentByUid(ctx context.Context, uid uuid.UUID) (*Payment, error)
	FetchAllPayments(ctx context.Context) ([]*Payment, error)
	DeletePaymentByUid(ctx context.Context, uid uuid.UUID) error
}
