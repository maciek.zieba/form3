package payment

import (
	"context"
	"fmt"
	"github.com/Rhymond/go-money"
	"github.com/google/uuid"
)

type (
	Payment struct {
		Uid         uuid.UUID
		Amount      *money.Money
		Description string
		OrderId     string
		Method      string
		Status      string
	}

	Service struct {
		repository Repository
	}
)

func NewService(repository Repository) *Service {
	return &Service{repository}
}

func (s Service) GetPayments(ctx context.Context) ([]*Payment, error) {
	p, err := s.repository.FetchAllPayments(ctx)
	if err != nil {
		return nil, InfrastructureError{fmt.Sprintf("Unable to fetch payments, reason: %s", err)}
	}
	return p, nil
}

func (s Service) DeletePaymentByUid(ctx context.Context, uid string) error {
	puid, err := uuid.Parse(uid)
	if err != nil {
		return DomainError{err.Error()}
	}
	p, err := s.repository.FetchPaymentByUid(ctx, puid)
	if err != nil {
		return InfrastructureError{fmt.Sprintf("Unable to fetch payment %s, reason: %s", uid, err.Error())}
	}
	if p == nil {
		return DomainError{fmt.Sprintf("Payment %s not found", uid)}
	}
	err = s.repository.DeletePaymentByUid(ctx, puid)
	if err != nil {
		return InfrastructureError{fmt.Sprintf("Unable to delete payment %s, reason: %s", uid, err.Error())}
	}
	return nil
}

func (s Service) GetPaymentByUid(ctx context.Context, uid string) (*Payment, error) {
	puid, err := uuid.Parse(uid)
	if err != nil {
		return nil, DomainError{err.Error()}
	}
	p, err := s.repository.FetchPaymentByUid(ctx, puid)
	if err != nil {
		return nil, InfrastructureError{fmt.Sprintf("Unable to fetch payment %s, reason: %s", uid, err.Error())}
	}
	return p, nil
}

func (s Service) CreateNewPayment(ctx context.Context, dto NewPaymentDTO) (*Payment, error) {
	amount := money.New(dto.Amount.Value, dto.Amount.CurrencyCode)
	if !IsPaymentMethodAllowed(dto.Method) {
		return nil, DomainError{fmt.Sprintf("payment method %s not allowed", dto.Method)}
	}
	payment := &Payment{
		Uid:         uuid.New(),
		Amount:      amount,
		OrderId:     dto.OrderId,
		Method:      dto.Method,
		Description: dto.Description,
		Status:      "Pending",
	}
	err := s.repository.PersistPayment(ctx, payment)
	if err != nil {
		return nil, InfrastructureError{fmt.Sprintf("Unable to persis payment %s, reason: %s", payment.Uid, err.Error())}
	}
	return payment, nil
}

func IsPaymentMethodAllowed(method string) bool {
	switch method {
	case
		"CreditCard",
		"PayPal":
		return true
	}
	return false
}
