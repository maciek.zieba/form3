package payment

type (
	InfrastructureError struct {
		message string
	}

	DomainError struct {
		message string
	}
)

func (e InfrastructureError) Error() string {
	return e.message
}

func (e DomainError) Error() string {
	return e.message
}
